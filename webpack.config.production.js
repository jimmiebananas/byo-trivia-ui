var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

    module: {
        loaders: [
            {
                test: /\.js(x?)$/,
                exclude: /(node_modules|vendor)/,
                loaders: ['babel']
            },
            {
                test: /\.(jpe(g?)|png|gif|svg|ico|cur)$/i,
                loader: 'file-loader?name=images/[name].[ext]'
            },
            {
                test: /new-fonts\/svg-fonts\/.*\.(?:eot|otf|svg|ttf|woff)(?:\?[a-z0-9=\.]+)?$/i,
                loader: 'file-loader?name=fonts/[name].[ext]'
            },
            {
                test: /\.(eot|otf|svg|ttf|woff|woff2)$/i,
                loader: 'file-loader?name=fonts/[name].[ext]'
            },
            {
                test: /\.s?css$/i,
                loader: 'style!css!sass?' + 'includePaths[]=' + (path.resolve(__dirname, './node_modules'))
            }
        ]
    },

    entry: {
        app: './src/index.js'
    },

    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'byo-trivia-ui.min.js'
    },

    devtool: '#source-map',

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Build Your Own Trivia',
            filename: 'index.html',
            template: 'src/index.template.ejs',
            favicon: ''
        })
    ],

    resolve: {
        root: ['node_modules'],
        extensions: ['', '.js', '.jsx']
    }
};