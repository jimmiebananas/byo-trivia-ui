import createReducer from '../utils/create-reducer';
import { appActionTypes, contestantActionTypes, hostActionTypes } from '../constants/action-types';
import { globalConstants } from '../constants/global-constants';
import * as appActions from '../actions/app-actions';
import * as contestantActions from '../actions/contestant-actions';

const initialState = {
    game: {
        started: false,
        contestants: [],
        audioState: {
            musicVolume: 'SEVEN',
            sfxVolume: 'SEVEN'
        }
    },
    isLoading: true,
    showAnswer: false,
};

export default createReducer(initialState, {

    [appActionTypes.GAME_STATE_UPDATED]: (state, action) => {

        const gameData = action.data;

        return {
            ...state,
            game: action.data,
            isLoading: false
        };
    },

    [hostActionTypes.TOGGLED_SHOW_ANSWER]: (state, action) => {
        return {
            ...state,
            showAnswer: action.data === true
        };
    }

});
