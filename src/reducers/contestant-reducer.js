import createReducer from '../utils/create-reducer';
import { appActionTypes, contestantActionTypes } from '../constants/action-types';
import { globalConstants } from '../constants/global-constants';
import * as appActions from '../actions/app-actions';
import * as contestantActions from '../actions/contestant-actions';

const initialState = {
    registered: false,
    openForRegistration: false,
    answering: false,
    answeringEnabled: false,
    score: 0,
    id: null,
    name: null,
    isLoading: true,
    errorMessage: null
};

export default createReducer(initialState, {

    [appActionTypes.GAME_STATE_UPDATED]: (state, action) => {

        const gameData = action.data;
        const { contestants, answeringContestant } = gameData;
        const currentContestant = getCurrentContestant(contestants);
        const registered = !!currentContestant.id;
        const answering = currentContestant.id === answeringContestant;
        const answeringContestantName = getAnsweringContestantName(contestants, answeringContestant);

        return {
            ...state,
            openForRegistration: gameData.openForRegistration,
            isLoading: false,
            id: currentContestant.id,
            name: currentContestant.name,
            answering: answering,
            answeringEnabled: currentContestant.answeringEnabled,
            score: currentContestant.score || 0,
            registered: registered,
            answeringContestantName: answeringContestantName
        };
    },

    [contestantActionTypes.REGISTERED]: (state, action) => {

        const { id, name, answeringEnabled, score } = action.data;

        return {
            ...state,
            id: id,
            name: name,
            answeringEnabled: answeringEnabled,
            score: score,
            registered: true,
            errorMessage: null
        };
    },

    [contestantActionTypes.REGISTRATION_FAILED]: (state, action) => {
        const { message } = action.data;

        return {
            ...state,
            errorMessage: message
        };
    }

});

const getCurrentContestant = (contestants) => {
    const contestantId = localStorage.getItem(globalConstants.contestantIdStorageKey);
    const currentContestant = contestants.find((contestant) => {
        return contestant.id === contestantId;
    });

    return currentContestant || {};
};

const getAnsweringContestantName = (contestants = [], answeringId) => {
    const answeringContestant = contestants.find((contestant) => {
        return contestant.id === answeringId;
    });

    return answeringContestant && answeringContestant.name;
};