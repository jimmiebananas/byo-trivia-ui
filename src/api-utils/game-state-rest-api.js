import $ from 'jquery';
import config from '../utils/config';

export function getGameState() {

    return new Promise((resolve, reject) => {
        $.ajax(`${config.restUrl}/gameState`, {
            method: 'GET',
            success: (res, status, xhr) => {
                resolve(res);
            },
            error: (xhr, status, error) => {
                reject(xhr);
            }
        });
    });
}

export function registerContestant(contestantName) {

    return new Promise((resolve, reject) => {
        $.ajax(`${config.restUrl}/register`, {
            method: 'POST',
            data: JSON.stringify({name: contestantName}),
            success: (res, status, xhr) => {
                resolve(res);
            },
            error: (xhr, status, error) => {
                reject(xhr);
            }
        });
    });
}