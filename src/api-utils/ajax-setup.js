import $ from 'jquery';

$.ajaxSetup({
    contentType: 'application/json'
});

$(document).ajaxSend(function (event, request, settings) {
    var contestantId = localStorage.getItem('contestantId');

    if (contestantId) {
        //request.setRequestHeader('byot-contestant-id', contestantId);
    }
});
