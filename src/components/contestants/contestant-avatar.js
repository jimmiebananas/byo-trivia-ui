import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ContestantAvatarControls } from '../../components';
import './_contestant-avatar.scss';

export default class ContestantAvatar extends Component {

    render() {
        const { name, answering, answeringEnabled, score, isHost } = this.props;

        let avatarClassName = 'contestant-avatar';
        let nameClassName = 'contestant-avatar__name';

        if (answering) {
            nameClassName += ' contestant-avatar__name--answering';
        }

        if (!answeringEnabled) {
            avatarClassName += ' contestant-avatar--not-allowed';
        }

        return (
            <div>
                <div className={avatarClassName}>
                    <div className={nameClassName}>{name}</div>
                    <div className='contestant-avatar__score'>{score}</div>
                </div>
                {isHost ? <ContestantAvatarControls {...this.props} /> : null}
            </div>
        );

    }

}