import React, { Component } from 'react';
import ContestantAvatar from './contestant-avatar';
import './_contestant-avatar.scss';

export default class ContestantsRow extends Component {

    render() {
        const { contestants, isHost } = this.props;
        const avatars = this.buildContestantsJsx(contestants);

        return (
            <div className='contestants-row'>
                {this.buildContestantsJsx(contestants)}
            </div>
        );
    }

    buildContestantsJsx = (contestants) => {
        const { isHost, hostUpdatedContestantScore, hostEnabledContestant, hostDisabledContestant } = this.props;
        const avatars = contestants.map((contestant) => {
            contestant.key = contestant.id;
            contestant.isHost = isHost;
            contestant.hostUpdatedContestantScore = hostUpdatedContestantScore;
            contestant.hostEnabledContestant = hostEnabledContestant;
            contestant.hostDisabledContestant = hostDisabledContestant;

            return (<ContestantAvatar {...contestant}/>);
        });

        return avatars;
    };

}