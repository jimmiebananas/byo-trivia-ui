import React, { Component } from 'react';
import './_welcome.scss';

export default class Welcome extends Component {

    render() {
        const { host } = this.props;

        return (
            <div className='welcome'>
                <div className='welcome__msg'>
                    Welcome to the trivia game!
                </div>
                <div className='welcome__msg'>
                    Visit the URL below to register
                </div>
                <div className='welcome__register-url'>
                    http://{host}:8080/play
                </div>
            </div>
        );
    }

}