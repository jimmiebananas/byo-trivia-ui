import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './_text-input.scss';

export default class TextInput extends Component {

    render() {

        const { placeholderText, maxLength } = this.props;
        let className = 'text-input__input';

        if (this.props.styleClassName) {
            className += ' ' + this.props.styleClassName;
        }

        return (
            <div className='text-input__wrapper'>
                <input
                    className={className}
                    placeholder={placeholderText}
                    maxLength={maxLength}
                    type='text'
                    ref='input'/>
            </div>
        );
    }

    getValue = () => {
        return ReactDOM.findDOMNode(this.refs.input).value;
    };

}