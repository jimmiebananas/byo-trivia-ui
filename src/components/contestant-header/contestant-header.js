import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './_contestant-header.scss';


export default class ContestantHeader extends Component {

    render() {

        const { name, score } = this.props;

        return (
            <div className='contestant-header'>
                <div className='contestant-header__name'>{name}</div>
                <div className='contestant-header__score'>Score: {score}</div>
            </div>
        );

    }

}