import React, { Component } from 'react';
import './_contestant-answering.scss';

export default class ContestantAnswering extends Component {

    render() {

        const { name } = this.props;

        return (
            <div className='contestant-answering'>
                <div className='contestant-answering__name'>
                    {name}
                </div>
                <div className='contestant-answering__content'>
                    is answering...
                </div>
            </div>
        );
    }
}