import React, { Component } from 'react';
import './_clues-list.scss';

export default class CluesList extends Component {

    render() {
        const { stylePrefix } = this.props;

        return (
            <div className={stylePrefix + '__clues-list'}>
                {this.buildCluesListJsx()}
            </div>
        );
    }

    buildCluesListJsx = () => {
        const { clues, stylePrefix } = this.props || [];

        const cluesListJsxArray = clues.map((clue, index) => {
            return (<div className={stylePrefix + '__clue'} key={index}>{clue}</div>);
        });

        return cluesListJsxArray;
    };

}