import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { TextInput, Button } from '../../components';
import './_contestant-register.scss';


export default class ContestantRegister extends Component {

    render() {

        return (
            <div className='contestant-register'>
                <TextInput styleClassName='contestant-register__input' ref='contestantNameInput' placeholderText='Enter player name' maxLength='20'/>
                <Button styleClassName='contestant-register__btn' clickHandler={this.registerContestant} text='Join Game' />
            </div>
        );

    }

    registerContestant = () => {
        const contestantName = this.refs.contestantNameInput.getValue();
        const { submittedRegistration } = this.props;

        if (contestantName.trim().length) {
            submittedRegistration(contestantName.trim());
        }
    };

}