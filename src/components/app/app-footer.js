import React, { Component } from 'react';
import './_app-footer.scss'

export default class AppFooter extends Component {

    render() {
        return (
            <div className='app-footer'>
                &copy; Jim Mowbray
            </div>
        );
    }

}