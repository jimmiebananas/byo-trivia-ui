import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { TextInput, Button } from '../../components';
import './_contestant-avatar-controls.scss';

export default class ContestantAvatarControls extends Component {

    render() {
        return (this.buildHostControls());
    }

    buildHostControls = () => {
        const { id, answeringEnabled, score } = this.props;
        const prefix = 'contestant-avatar-controls';


        return (
            <div className={prefix}>
                <div className={prefix + '__enable-disable-wrapper'}>
                    <Button disabled={answeringEnabled} clickHandler={this.handleEnable} styleClassName={prefix + '__btn'} text='enable' />
                    <Button disabled={!answeringEnabled} clickHandler={this.handleDisable} styleClassName={prefix + '__btn'} text='disable' />
                </div>
                <div className={prefix + '__edit-score-wrapper'}>
                    <Button styleClassName={prefix + '__edit-score-btn'} clickHandler={this.handleUpdateScore} text='Update' />
                    <TextInput ref='editScore' defaultValue={score} styleClassName={prefix + '__edit-score-input'} />
                </div>
            </div>
        );
    };

    handleUpdateScore = () => {
        const { name, id, hostUpdatedContestantScore } = this.props;
        const updatedScore = this.refs.editScore.getValue();
        let confirmed = confirm('Change score for ' + name + ' to ' + updatedScore + '?');

        if (confirmed) {
            hostUpdatedContestantScore(id, updatedScore);
        }
    };

    handleEnable = () => {
        const { name, id, hostEnabledContestant } = this.props;
        let confirmed = confirm('Enable ' + name + '?');

        if (confirmed) {
            hostEnabledContestant(id);
        }
    };

    handleDisable = () => {
        const { name, id, hostDisabledContestant } = this.props;
        let confirmed = confirm('Disable ' + name + '?');

        if (confirmed) {
            hostDisabledContestant(id);
        }
    };
}