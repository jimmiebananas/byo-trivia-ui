import React, { Component } from 'react';
import './_volume-control.scss';

const levelsMap = {MUTE: 0, ONE: 1, TWO: 2, THREE: 3, FOUR: 4, FIVE: 5, SIX: 6, SEVEN: 7, EIGHT: 8, NINE: 9, TEN: 10};

export default class VolumeControl extends Component {

    render() {
        const levelClasses = this.buildClasses();
        const { level, name } = this.props;
        const muteLabel = 'Mute' + name;
        let muteClass = 'volume-control__mute';
        if (level === 'MUTE') {
            muteClass += ' volume-control__bars--filled';
        }

        return (
            <div className='volume-control__wrapper'>

                <div className='volume-control__mute-wrapper'>
                    <div className='volume-control__name'>{name}</div>
                    <div className={muteClass} onClick={this.handleClick} data-level='MUTE'>Mute</div>
                </div>
                <div className='volume-control__bars'>
                    <div className={levelClasses[1]} onClick={this.handleClick} data-level='ONE'/>
                    <div className={levelClasses[2]} onClick={this.handleClick} data-level='TWO'/>
                    <div className={levelClasses[3]} onClick={this.handleClick} data-level='THREE'/>
                    <div className={levelClasses[4]} onClick={this.handleClick} data-level='FOUR'/>
                    <div className={levelClasses[5]} onClick={this.handleClick} data-level='FIVE'/>
                    <div className={levelClasses[6]} onClick={this.handleClick} data-level='SIX'/>
                    <div className={levelClasses[7]} onClick={this.handleClick} data-level='SEVEN'/>
                    <div className={levelClasses[8]} onClick={this.handleClick} data-level='EIGHT'/>
                    <div className={levelClasses[9]} onClick={this.handleClick} data-level='NINE'/>
                    <div className={levelClasses[10]} onClick={this.handleClick} data-level='TEN'/>
                </div>
            </div>
        );
    }

    buildClasses = () => {
        const { level } = this.props;
        const levels = ['MUTE', 'ONE', 'TWO', 'THREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN', 'EIGHT', 'NINE', 'TEN'];

        return levels.map((levelName, index) => {
            const numericLevel = index;
            let className = 'volume-control__bar-' + levelName.toLowerCase();
            if (levelsMap[level] >= numericLevel) {
                className += ' volume-control__bars--filled';
            }

            return className;
        });
    };

    handleClick = (clickEvent) => {
        const selectedLevel = clickEvent.target.dataset.level;
        const { handler } = this.props;
        handler(selectedLevel);
    }

}