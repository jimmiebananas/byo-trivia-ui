import React, { Component } from 'react';
import { Button } from '../../components';
import './_buzzer.scss';

export default class Buzzer extends Component {

    render() {
        const isDisabled = this.props.disabled === true;
        const { text, answering } = this.props;
        let btnStyle = 'buzzer__btn';

        if (isDisabled) {
            btnStyle += ' buzzer__btn--disabled';
        }

        if (answering) {
            btnStyle += ' buzzer__btn--answering';
        }

        return (
            <div className='buzzer__wrapper'>
                <Button
                    styleClassName={btnStyle}
                    clickHandler={this.handleClick}
                    text={text}
                    disabled={isDisabled}
                />
            </div>
        );
    }

    handleClick = () => {
        if (this.props.disabled === true) {
            return;
        }
        const { contestantBuzzedIn } = this.props;
        contestantBuzzedIn(this.props.contestantId);
    };

}