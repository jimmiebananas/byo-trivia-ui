import createActionTypes from '../utils/create-action-types';

export const appActionTypes = createActionTypes('APP_ACTIONS', [
    'LOADED_APPLICATION',
    'GAME_STATE_UPDATED'
]);

export const contestantActionTypes = createActionTypes('CONTESTANT_ACTIONS', [
    'SUBMITTED_REGISTRATION',
    'REGISTERED',
    'REGISTRATION_FAILED',
    'BUZZED_IN'
]);

export const hostActionTypes = createActionTypes('HOST_ACTIONS', [
    'CREATED_GAME',
    'STARTED_GAME',
    'RESET_GAME',
    'CONTINUED_GAME',
    'ACCEPTED_ANSWER',
    'DECLINED_ANSWER',
    'NO_SCORED_ANSWER',
    'UPDATED_CONTESTANT_SCORE',
    'ENABLED_CONTESTANT',
    'DISABLED_CONTESTANT',
    'TOGGLED_SHOW_ANSWER',
    'SET_MUSIC_VOLUME',
    'SET_SFX_VOLUME'
]);