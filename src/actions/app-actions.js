import $ from 'jquery';
import { appActionTypes, contestantActionTypes, hostActionTypes } from '../constants/action-types';
import * as gameRestApi from '../api-utils/game-state-rest-api';
import { Stomp } from '../../vendor/stomp-client';
import config from '../utils/config';

let socketClient;

export function loadedApplication(data) {

    return dispatch => {
        dispatch({
            type: appActionTypes.LOADED_APPLICATION
        });

        gameRestApi.getGameState()
            .then(
                gameState => {
                    dispatch(gameStateUpdated(gameState)),
                    dispatch(openSocketConnection())
                },
                error => console.log('We got a problem', error)
            )
            .catch((err) => {
                setTimeout(() => { throw err; });
            });
    }
}

export function gameStateUpdated(gameState) {
    return {
        type: appActionTypes.GAME_STATE_UPDATED,
        data: gameState
    };
}

export function openSocketConnection() {

    return dispatch => {

        const socket = new WebSocket(`${config.socketUrl}/gameSocket`);
        socketClient = new Stomp.over(socket);
        socketClient.debug = null;

        socketClient.connect({}, function (connectedFrame) {
            console.log('socket opened...');
            socketClient.subscribe('/update', function (frame) {
                dispatch(gameStateUpdated(JSON.parse(frame.body)));
            });
        });
    };
}

export function contestantBuzzedIn(contestantId) {

    return dispatch => {
        dispatch({
            type: contestantActionTypes.BUZZED_IN,
            data: contestantId
        });

        socketClient.send('/app/buzzIn', {}, JSON.stringify(contestantId));
    };
}

export function hostCreatedGame() {
    return dispatch => {
        dispatch({
            type: hostActionTypes.CREATED_GAME
        });

        socketClient.send('/app/createGame', {}, '');
    }
}

export function hostStartedGame(questionFilename) {
    return dispatch => {
        dispatch({
            type: hostActionTypes.STARTED_GAME
        });

        socketClient.send('/app/startGame', {}, JSON.stringify(questionFilename));
    };
}

export function hostContinuedGame() {
    return dispatch => {
        dispatch({
            type: hostActionTypes.CONTINUED_GAME
        });

        socketClient.send('/app/continueGame', {}, '');
    };
}

export function hostAcceptedAnswer(contestantId) {
    return dispatch => {
        dispatch({
            type: hostActionTypes.ACCEPTED_ANSWER
        });

        socketClient.send('/app/acceptAnswer', {}, JSON.stringify(contestantId));
    };
}

export function hostDeclinedAnswer(contestantId) {
    return dispatch => {
        dispatch({
            type: hostActionTypes.DECLINED_ANSWER
        });

        socketClient.send('/app/declineAnswer', {}, JSON.stringify(contestantId));
    };
}

export function hostResetGame(questionFilename) {
    return dispatch => {
        dispatch({
            type: hostActionTypes.RESET_GAME
        });

        socketClient.send('/app/resetGame', {}, JSON.stringify(questionFilename));
    };
}

export function hostNoScoredAnswer() {
    return dispatch => {
        dispatch({
            type: hostActionTypes.NO_SCORED_ANSWER
        });

        socketClient.send('/app/noScoreAnswer', {}, '');
    };
}

export function hostUpdatedContestantScore(contestantId, score) {
    return dispatch => {
        dispatch({
            type: hostActionTypes.UPDATED_CONTESTANT_SCORE
        });

        socketClient.send('/app/setScore', {}, JSON.stringify({contestantId: contestantId, score: score}));
    };
}

export function hostEnabledContestant(contestantId) {
    return dispatch => {
        dispatch({
            type: hostActionTypes.ENABLED_CONTESTANT
        });

        socketClient.send('/app/enableContestant', {}, JSON.stringify(contestantId));
    };
}

export function hostDisabledContestant(contestantId) {
    return dispatch => {
        dispatch({
            type: hostActionTypes.DISABLED_CONTESTANT
        });

        socketClient.send('/app/disableContestant', {}, JSON.stringify(contestantId));
    };
}

export function hostToggledShowAnswer(showAnswer) {
    return {
        type: hostActionTypes.TOGGLED_SHOW_ANSWER,
        data: showAnswer
    };
}

export function hostSetMusicVolume(level) {
    return dispatch => {
        dispatch({
            type: hostActionTypes.SET_MUSIC_VOLUME
        });

        socketClient.send('/app/setMusicVolume', {}, JSON.stringify({volume: level}));
    }
}

export function hostSetSfxVolume(level) {
    return dispatch => {
        dispatch({
            type: hostActionTypes.SET_MUSIC_VOLUME
        });

        socketClient.send('/app/setSfxVolume', {}, JSON.stringify({volume: level}));
    }
}