import $ from 'jquery';
import { appActionTypes, contestantActionTypes } from '../constants/action-types';
import { globalConstants } from '../constants/global-constants';
import * as gameRestApi from '../api-utils/game-state-rest-api';
import { Stomp } from '../../vendor/stomp-client';
import config from '../utils/config';

export function submittedRegistration(contestantName) {

    return dispatch => {
        dispatch({
            type: contestantActionTypes.SUBMITTED_REGISTRATION
        });

        gameRestApi.registerContestant(contestantName)
            .then(
                registeredContestant => {
                    dispatch(newContestantRegistered(registeredContestant))
                },
                (erroredXhr) => {
                    if (erroredXhr.status === 400) {
                        dispatch(contestantRegistrationFailed(erroredXhr.responseJSON.message));
                    }
                }
            )
            .catch((err) => {
                setTimeout(() => { console.log(err); throw err; });
            });
    }
}

export function newContestantRegistered(registeredContestant) {
    localStorage.setItem(globalConstants.contestantIdStorageKey, registeredContestant.id);

    return {
        type: contestantActionTypes.REGISTERED,
        data: registeredContestant
    };
}

export function contestantRegistrationFailed(message) {
    return {
        type: contestantActionTypes.REGISTRATION_FAILED,
        data: { message: message }
    };
}
