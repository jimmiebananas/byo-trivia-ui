export default {
    restUrl: 'http://' + window.location.hostname + ':8080',
    socketUrl: 'ws://' + window.location.hostname + ':8080',
}