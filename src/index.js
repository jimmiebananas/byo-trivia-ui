
/* React  and Redux */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configure-store';

/* Application-specific dependencies */
import { AppRouterConnector } from './views';

import * as images from './assets/images/';

import './api-utils/ajax-setup';
import './assets/styles/style.scss';
import * as vendor from '../vendor';

// Creates the top-level application store with middleware. Exports the store
// for use in other modules.
const store = configureStore();

// Renders the application to the DOM
ReactDOM.render(
    <Provider store={store}>
        <AppRouterConnector store={store} />
    </Provider>,
    document.getElementById('byo-trivia-app-wrapper')
);

