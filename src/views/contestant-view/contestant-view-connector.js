import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import * as contestantActions from '../../actions/contestant-actions';
import * as appActions from '../../actions/app-actions';
import { connect } from 'react-redux';
import ContestantView from './contestant-view';

function select(state) {

    return {
        game: state.game,
        contestant: state.contestant
    };
}

class ContestantViewConnector extends Component {

    render() {
        const { dispatch } = this.props;

        return (
            <ContestantView
                {...this.props.game}
                {...this.props.contestant}
                {...bindActionCreators(contestantActions, dispatch)}
                {...bindActionCreators(appActions, dispatch)}
            />
        );
    }

}

export default connect(select)(ContestantViewConnector);