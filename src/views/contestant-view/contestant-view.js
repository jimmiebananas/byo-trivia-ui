import React, { Component } from 'react';
import { globalConstants } from '../../constants/global-constants';
import { ContestantRegister, Buzzer, ContestantHeader, CluesList } from '../../components';
import './_contestant-view.scss';

export default class ContestantView extends Component {

    render() {
        const { isLoading } = this.props;

        return (
            <div>
                {isLoading ? this.buildLoadingJsx() : this.buildContestantJsx()}
            </div>
        );
    }

    buildContestantJsx = () => {
        const { registered, openForRegistration, game, errorMessage } = this.props;
        let jsxToShow;

        if (registered && game.started) {
            jsxToShow = this.buildGameInProgressJsx;
        } else if (registered) {
            jsxToShow = this.buildAwaitingOtherContestantsJsx;
        } else {
            jsxToShow = openForRegistration ? this.buildRegistrationFormJsx : this.buildRegistrationClosedJsx;
        }

        return (
            <div>
                {errorMessage ? this.buildErrorMessage() : null}
                {jsxToShow()}
            </div>
        );
    };

    buildErrorMessage = () => {
        return (
            <div style={{color: 'red', 'margin-bottom': '8px'}}>{this.props.errorMessage}</div>
        );
    };

    buildGameInProgressJsx = () => {
        const {game, id, contestantBuzzedIn, name, score } = this.props;
        const transitionOrNoClues = game.stage === 'DISPLAYING_TRANSITION' || (game.currentQuestion && game.currentQuestion.clues.length === 0);
        const buzzerState = this.determineBuzzerState();

        return (
            <div>
                <ContestantHeader name={name} score={score} />
                <Buzzer
                    contestantBuzzedIn={contestantBuzzedIn}
                    contestantId={id}
                    disabled={!buzzerState.enabled}
                    text={buzzerState.text}
                    answering={buzzerState.answering}
                />
                {!transitionOrNoClues ? this.buildCluesJsx() : null}
            </div>
        );
    };

    determineBuzzerState = () => {
        const { answering, answeringEnabled, game, answeringContestantName } = this.props;
        const someoneElseAnswering = !answering && game.answeringContestant != null;
        const waitingForClues = game.stage === 'DISPLAYING_TRANSITION' || (game.currentQuestion && game.currentQuestion.clues.length === 0);
        let buzzerEnabled = true;
        let buzzerText = 'Buzz In!';
        let isAnswering = false;

        if (game.stage === 'DISPLAYING_ANSWER') {
            buzzerEnabled = false;
            buzzerText = 'Waiting for next question';
        } else if (answering) {
            buzzerEnabled = false;
            buzzerText = 'You are answering!';
            isAnswering = true;
        } else if (someoneElseAnswering) {
            buzzerEnabled = false;
            buzzerText = `${answeringContestantName} is answering`;
        } else if (waitingForClues) {
            buzzerEnabled = false;
            buzzerText = 'Waiting for clues';
        } else if (!answeringEnabled) {
            buzzerEnabled = false;
            buzzerText = 'You already buzzed in this round';
        }

        return {enabled: buzzerEnabled, text: buzzerText, answering: isAnswering};
    };

    buildLoadingJsx = () => {
        return (<div>Reading game state...</div>);
    };

    buildCluesJsx = () => {
        const clues = this.props.game.currentQuestion.clues || [];
        const answer = this.props.game.stage === 'DISPLAYING_ANSWER' && this.props.game.currentQuestion.answer;

        return (
            <div>
                {answer ? <div className='contestant-view__clues-answer'>{answer}</div> : null}
                <CluesList clues={clues} stylePrefix='contestant-view' />
            </div>
        );
    };

    buildRegistrationFormJsx = () => {
        return (<ContestantRegister submittedRegistration={this.props.submittedRegistration}/>);
    };

    buildRegistrationClosedJsx = () => {
        return (<div>Sorry, the current game is not open for registration</div>);
    };

    buildAwaitingOtherContestantsJsx = () => {
        return (<div>You're in! Waiting for the game to start.</div>);
    };

}