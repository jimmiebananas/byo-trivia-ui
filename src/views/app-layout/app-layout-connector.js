import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import * as appActions from '../../actions/app-actions';
import { AppFooter, AppHeader } from '../../components';
import './_app-layout.scss';


const select = createStructuredSelector({
    router: state => state.router
});

const hideHeaderRoutes = ['/play'];

export class AppLayoutConnector extends Component {

    render() {

        const { dispatch, children, router: { location: { pathname } } } = this.props;
        const hideHeaderFooter = hideHeaderRoutes.indexOf(pathname.toLowerCase()) > -1;

        return (
            <div className='app-layout' {...bindActionCreators(appActions, dispatch)}>
                {!hideHeaderFooter ? <AppHeader /> : null}

                <main id='app-layout' className='app-layout__content'>
                    {children}
                </main>

                {!hideHeaderFooter ? <AppFooter /> : null}
            </div>
        );
    }

}

export default connect(select)(AppLayoutConnector);