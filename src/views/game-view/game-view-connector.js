import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import GameView from './game-view';

function select(state) {

    return {
        game: state.game
    };
}

class GameViewConnector extends Component {

    render() {
        const { dispatch } = this.props;

        return (
            <GameView
                {...this.props.game}
            />
        );
    }

}

export default connect(select)(GameViewConnector);