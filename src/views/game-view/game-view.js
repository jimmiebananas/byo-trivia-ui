import React, { Component } from 'react';
import { ContestantsRow, ContestantAnswering, Welcome } from '../../components';
import { AppModal } from '../../views';
import './_game-view.scss';

export default class GameView extends Component {

    render() {
        console.log('props in game view', this.props);

        const { game, isLoading } = this.props || {};

        return (
            <div>{isLoading ? this.buildLoadingJsx() : this.buildGameJsx()}</div>
        );
    }

    buildGameJsx = () => {
        const { game, isHost, showAnswer } = this.props;
        const { host } = game;

        let showClues = game.stage === 'DISPLAYING_QUESTION' || game.stage === 'ANSWERING_QUESTION' || game.stage === 'DISPLAYING_ANSWER';
        let showContestants = true;
        const currentQId = game.currentQuestion ? game.currentQuestion.id : 1;
        let bannerText = 'http://' + host + ':8080/play';
        const revealingAnswer = game.stage === 'DISPLAYING_ANSWER';
        let bannerClass = 'game-view__banner';
        if (revealingAnswer) {
            bannerClass += ' game-view__banner--answer';
        }
        const answeringContestantName = this.getAnsweringContestantName();

        if (game.stage === 'DISPLAYING_QUESTION' || game.stage === 'ANSWERING_QUESTION') {
            bannerText = isHost && showAnswer ? game.currentQuestion.answer : '????????';
        } else if (game.stage === 'DISPLAYING_ANSWER') {
            bannerText = game.currentQuestion.answer;
        } else if (game.stage === 'DISPLAYING_TRANSITION') {
            bannerText = 'Question ' + currentQId;
        } else if (game.stage === 'ENDED') {
            bannerText = 'Game Over!';
        }

        return (
            <div>
                <AppModal
                    isVisible={game.stage === 'ANSWERING_QUESTION' && !this.props.isHost}
                    content={<ContestantAnswering name={answeringContestantName} />}
                />
                <div className='contestants-view'>
                    {showContestants ? this.buildContestantsRowJsx(game.contestants) : null}
                </div>
                <div className='game-view'>
                    {game.stage === 'CREATED' ? <Welcome host={host} /> : <div className={bannerClass}>{bannerText}</div>}
                    {showClues ? this.buildQuestionCluesJsx(game.currentQuestion.clues) : null}
                </div>
            </div>
        );
    };

    buildQuestionCluesJsx = (clues = []) => {
        const cluesJsxArray = clues.map((clue, index) => {
            let clueClass = 'game-view__clue';
            if (this.props.isHost) {
                clueClass += ' game-view__clue--host';
            }

            return (<div className={clueClass} key={index}>{clue}</div>);
        });

        let listClass = 'game-view__clues-list';
        if (this.props.isHost) {
            listClass += ' game-view__clues-list--host';
        }

        return (
            <div className={listClass}>
                {cluesJsxArray}
            </div>
        );
    };

    buildContestantsRowJsx = (contestants) => {
        const { isHost, hostUpdatedContestantScore, hostEnabledContestant, hostDisabledContestant } = this.props;
        const contestantsRowProps = {
            contestants: contestants,
            isHost: isHost,
            hostUpdatedContestantScore: hostUpdatedContestantScore,
            hostEnabledContestant: hostEnabledContestant,
            hostDisabledContestant: hostDisabledContestant
        };

        return (
            <div>
                <ContestantsRow {...contestantsRowProps} />
            </div>
        );
    };

    buildLoadingJsx = () => {
        return (<div>Reading game state...</div>);
    };

    getAnsweringContestantName = () => {
        const contestants = this.props.game.contestants || [];
        const answeringId = this.props.game.answeringContestant;

        const answeringContestant = contestants.find((contestant) => {
            return contestant.id === answeringId;
        });

        return answeringContestant && answeringContestant.name;
    }
}