import React, { Component } from 'react';

import { Router, Route, Redirect, IndexRoute } from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ReduxRouter } from 'redux-router';

import AppLayoutConnector from './app-layout/app-layout-connector';
import * as appActions from '../actions/app-actions';
import * as components from '../components';
import * as views from '../views';

const history = new createBrowserHistory();

export default class AppRouterConnector extends Component {

    componentDidMount() {
        const { store: { dispatch } } = this.props;
        dispatch(appActions.loadedApplication());
    }

    enterGame = (nextState, replaceState) => {
        console.log('entering gameViewConnector');
    };

    render() {
        return (
            <ReduxRouter>
                <Route component={AppLayoutConnector}>
                    <Route path='game' component={views.GameViewConnector} header='Game' />
                    <Route path='play' component={views.ContestantViewConnector} header='Play' />
                    <Route path='host' component={views.HostViewConnector} header='Host' />
                </Route>

                <Redirect from='*' to='/game' />
            </ReduxRouter>
        );
    }
}