import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as hostActions from '../../actions/app-actions';
import HostView from './host-view';

function select(state) {

    return {
        game: state.game
    };
}

class HostViewConnector extends Component {

    render() {
        const { dispatch } = this.props;

        return (
            <HostView
                {...this.props.game}
                {...bindActionCreators(hostActions, dispatch)}
            />
        );
    }

}

export default connect(select)(HostViewConnector);