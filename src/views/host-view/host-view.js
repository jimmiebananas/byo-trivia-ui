import React, { Component } from 'react';
import GameView from '../../views/game-view/game-view';
import HostControlsView from '../../views/host-view/host-controls-view';

export default class HostView extends Component {

    createGameClicked = () => {
        const { hostCreatedGame } = this.props;
        hostCreatedGame();
    }

    buildNotCreatedJsx = () => {
        return (
            <div>
                <div>To create a game, click the button below</div>
                <button onClick={this.createGameClicked}>Create Game</button>
            </div>
        );
    }

    buildHostGameJsx = () => {
        const hostProps = {
            ...this.props,
            isHost: true
        };

        return (
            <div>
                <HostControlsView {...hostProps}/>
                <GameView {...hostProps}/>
            </div>
        );
    }

    render() {
        if (!this.props.isLoading && this.props.game.stage === 'NOT_CREATED') {
            return this.buildNotCreatedJsx();
        } else {
            return this.buildHostGameJsx();
        }
    }
}