import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './_host-controls-view.scss';
import { VolumeControl } from '../../components';

export default class HostControlsView extends Component {
    render() {
        const stage = this.props.game.stage;
        const { audioState } = this.props.game;
        const hasContestants = !!this.props.game.contestants.length;
        const showAnswering = stage === 'ANSWERING_QUESTION';
        const showContinue = !showAnswering &&
            (stage === 'DISPLAYING_QUESTION' || stage === 'DISPLAYING_ANSWER' || stage === 'DISPLAYING_TRANSITION');

        const showAnswer = this.props.showAnswer;
        const contestants = this.props.game.contestants;
        const stillAnswerable = this.props.game.contestants.reduce((prev, curr) => {
            return !!prev || curr.answeringEnabled;
        }, false);

        const showNoScore = stage !== 'DISPLAYING_ANSWER' && stage != 'ANSWERING_QUESTION'
            && this.props.game.currentQuestion && this.props.game.currentQuestion.clues.length > 0;

        return (
            <div className='host-controls-view'>
                <div className='host-controls-view__controls'>
                    <div className='host-controls-view__btns'>
                        <button
                            className='host-controls-view__btn'
                            disabled={stage !== 'CREATED' || !hasContestants}
                            onClick={this.handleStartClicked}>
                            Start
                        </button>
                        <button
                            className='host-controls-view__btn'
                            disabled={!showContinue}
                            onClick={this.handleContinueClicked}>
                            Continue
                        </button>
                        <button
                            className='host-controls-view__btn'
                            disabled={!showNoScore}
                            onClick={this.handleNoScoreClicked}>
                            No Score
                        </button>
                        <button
                            className='host-controls-view__btn'
                            disabled={!showAnswering}
                            onClick={this.handleAcceptAnswerClicked}>
                            Accept
                        </button>
                        <button
                            className='host-controls-view__btn'
                            disabled={!showAnswering}
                            onClick={this.handleDeclineAnswerClicked}>
                            Decline
                        </button>
                        <input
                            className='host-controls-view__show-answer'
                            onClick={this.handleShowAnswerClicked}
                            type='checkbox'
                            defaultChecked={showAnswer}/> Show Answer
                    </div>
                    <div className='host-controls-view__volume'>
                        <VolumeControl name='Music' level={audioState.musicVolume} handler={this.props.hostSetMusicVolume}/>
                    </div>
                    <div className='host-controls-view__volume'>
                        <VolumeControl name='SFX' level={audioState.sfxVolume} handler={this.props.hostSetSfxVolume} />
                    </div>
                </div>
                <div className='host-controls-view__question-set-select'>
                    {this.buildQuestionSetsDropdown()}
                </div>
                <div className='host-controls-view__reset'>
                    {this.buildResetGameJsx()}
                </div>
            </div>
        );
    }

    buildQuestionSetsDropdown = () => {
        const questionSets = this.props.game.questionFilenames || [];
        const selectedSet = this.props.game.currentQuestionFilename;

        const optsJsxArray = questionSets.map((filename, index) => {
            return (<option key={index}>{filename}</option>);
        });

        return (
            <select ref='questionSet' defaultValue={selectedSet}>
                {optsJsxArray}
            </select>
        );
    };

    handleStartClicked = () => {
        const { hostStartedGame } = this.props;
        const dropdown = ReactDOM.findDOMNode(this.refs.questionSet);

        hostStartedGame(dropdown.value);
    };

    handleContinueClicked = () => {
        const { hostContinuedGame } = this.props;
        hostContinuedGame();
    };

    handleNoScoreClicked = () => {
        const { hostNoScoredAnswer } = this.props;
        hostNoScoredAnswer();
    };

    handleAcceptAnswerClicked = () => {
        const answeringContestantId = this.props.game.answeringContestant;
        const { hostAcceptedAnswer } = this.props;
        hostAcceptedAnswer(answeringContestantId);
    };

    handleDeclineAnswerClicked = () => {
        const answeringContestantId = this.props.game.answeringContestant;
        const { hostDeclinedAnswer } = this.props;
        hostDeclinedAnswer(answeringContestantId);
    };

    buildResetGameJsx() {
        return (
            <button onClick={this.handleResetClicked} className='host-view__reset-btn'>
                Reset Game
            </button>
        );
    };

    handleResetClicked = () => {
        let confirmed = confirm('Resetting the game will lose all current progress.\n\n Are you sure you want to reset?');
        const { hostResetGame } = this.props;
        const dropdown = ReactDOM.findDOMNode(this.refs.questionSet);

        if (confirmed) {
            hostResetGame(dropdown.value);
        }
    };

    handleShowAnswerClicked = (checkboxClickEvent) => {
        const { hostToggledShowAnswer } = this.props;

        hostToggledShowAnswer(checkboxClickEvent.target.checked);
    };
}