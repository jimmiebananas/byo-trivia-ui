import React, { Component } from 'react';
import './_app-modal.scss';

export default class AppModal extends Component {

    render() {
        const { content, isVisible } = this.props;
        let overlayClass = 'app-modal__overlay';
        let contentClass = 'app-modal__content';

        if (isVisible === true) {
            overlayClass += ' app-modal__overlay--visible';
            contentClass += ' app-modal__content--visible';
        }


        return (
            <div>
                <div className={overlayClass}></div>
                <div className={contentClass}>
                    {content}
                </div>
            </div>
        );
    }
}